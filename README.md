# Simple Application
Files for building sample java app in a CI/CD processing sequence

See [manumazu/projet_ansible](https://gitlab.com/manumazu/projet_ansible) for some Ansible deployment example

## License
Developed under [the MIT license](https://opensource.org/licenses/MIT).



